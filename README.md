# blockchain-documentation

### Clone repository

``` bash
$ git clone git@gitlab.com:oscity/singletally-documentation.git
```
or 

``` bash
$ git clone https://gitlab.com/oscity/singletally-documentation.git
```

### Website
``` bash
$ cd website
```

### Install dependencies

In website:

``` bash
$ yarn install
```

### Run project

In website:

``` bash
$ yarn start
```